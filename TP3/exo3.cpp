#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->left = NULL;
        this->right = NULL;
        this->value = value;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        //si value plus petit que node alors on insert à gauche, sinon c'est à droite
        if(this->value > value){
            //regarder si il a un enfnt gauche
            if(this->left == NULL){
                //creer un nouveau noeud
                this->left = new SearchTreeNode(value);
                this->left->initNode(value);    
            }
            else{
                this->left->insertNumber(value);
            }
        }
        else{
            //on fait pareil si c'est à droite
              if(this->right == NULL){
                //creer un nouveau noeud
                this->right = new SearchTreeNode(value);
                this->right->initNode(value);    
            }
            else{
                this->right->insertNumber(value);
            }
        }
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        //on regarde si c'est une feuille
        if(this->isLeaf()){
            return 1;    
        }
        else{ //en fait on fait comme un nodesCount de chaque cote et on compare
            int hauteurGauche = 0;
            int hauteurDroite = 0;

            if(this->left != NULL){
                hauteurGauche = this->left->height();
            }

            if(this->right !=NULL){
                hauteurDroite = this->right->height();
            }

            if(hauteurGauche>hauteurDroite){
                return 1+ hauteurGauche;
            }
            else{
                return 1+hauteurDroite;
            }
        }
        
    }

    int balance(){
        //compte les noeuds des deux cotés et renvoie la difference
        int hauteurGauche;
        int hauteurDroite;

        if(this->left != NULL){
            hauteurGauche = this->left->height();
        }
        else{
            hauteurGauche=0;
        }
        if (this->right!= NULL)
        {
            hauteurDroite = this->right->height();
        }
        

        return hauteurDroite-hauteurGauche;
    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        //on regarde si c une feuille
        if(this->isLeaf()){
           return 1; 
        }
        else{
            //on va devoir compter du coté gauche et du coté droit séparément + surement devoir faire une récursion ???
            int hauteurGauche=0;
            int hauteurDroite=0;
            //on regarde du coté gauche d'abord
            if(this->left != NULL){ //si il a des enfants alors on doit regarder si 'est une feuille et faire pareil pour les enfants bref faut appeler nodescount
               hauteurGauche = this->left->nodesCount();
            }
            //on fait pareil à droite
            if(this->right != NULL){
                hauteurDroite = this->right->nodesCount();
            }
            return 1 + hauteurGauche + hauteurDroite; 

        }
        
	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        return (this->left == NULL && this->right == NULL);
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        if(this->isLeaf()){
            leaves[leavesCount]=this;
            leavesCount++;
        }
        else{
            if(this->left != NULL){
                this->left->allLeaves(leaves, leavesCount);
            }
            if(this->right != NULL){
                this->right->   allLeaves(leaves, leavesCount);
            }
        }
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        //ok en fait on fait pareil que allleaves mais dans l'ordre lol
        
        //au debut je voulais verifier si c pas null comme allLeaves mais ça sera verifié par le if de gauche et de droite donc c totalement ok
        //faut le faire entre les deux if quoi c logique finalement

        //gauche les plus petits
        if(this->left != NULL){
            this->left->inorderTravel(nodes, nodesCount);
        }

        nodes[nodesCount]= this;
        nodesCount++; // voila c ici qu'il faut faire ça mgl

        if(this->right !=NULL){
            this->right->inorderTravel(nodes, nodesCount);
        }

	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        //bon pour le coup ça c'est exactement comme allLeaves mais on verifie pas si c'est une feuille comme ça on demarre des parents
        nodes[nodesCount]= this;
        nodesCount++;
        
        if(this->left != NULL){
            this->left->preorderTravel(nodes,nodesCount);
        }

        if(this->right != NULL){
            this->right-> preorderTravel(nodes,nodesCount);
        }
        
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        //here we go again
        if(this->left != NULL){
            this->left->postorderTravel(nodes,nodesCount);
        }

        if(this->right != NULL){
            this->right->postorderTravel(nodes,nodesCount);
        }

        nodes[nodesCount]=this;
        nodesCount++;

	}

	Node* find(int value) {
        // find the node containing value
        //bon, deja on va faire le cas simple cad si c'est une feuille on a juste un truc à tester
        if(this->isLeaf() && this->value != value){
		return nullptr;
        }
        else{ //merci la recursvité ptn
            if(this->value == value){ //tjrs le cas simple en premier
                return this;
            }
            else{
                if(value < this->value){
                    if(this->left != NULL){
                        this->left->find(value);
                    }
                    else{
                        return nullptr;
                    }
                }
                else{
                    if(this->right != NULL){
                        this->right->find(value);
                    }
                    else{
                        return nullptr;
                    }
                }
            }

        }
	}



    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

// SearchTreeNode* createNode(int value) {
//     return new SearchTreeNode(value);
// }

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
