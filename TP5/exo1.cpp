#include <tp5.h>
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;


std::vector<string> TP5::names(
{
    "Yolo", "Anastasiya", "Clement", "Sirine", "Julien", "Sacha", "Leo", "Margot",
    "JoLeClodo", "Anais", "Jolan", "Marie", "Cindy", "Flavien", "Tanguy", "Audrey",
    "Mr.PeanutButter", "Bojack", "Mugiwara", "Sully", "Solem",
    "Leo", "Nils", "Vincent", "Paul", "Zoe", "Julien", "Matteo",
    "Fanny", "Jeanne", "Elo"
});


int HashTable::hash(std::string element)
{
    // use this->size() to get HashTable size
    int ascii = (int)(element[0]); //recup code ascii
    if(ascii > this->size()-1){ //verifie si c + que le +grand indice du tableau
        ascii = ascii%this->size(); //reste
    }
    return ascii;
}

void HashTable::insert(std::string element)
{
    // use (*this)[i] or this->get(i) to get a value at index i
    int index = hash(element);
    this->set(index, element); //g un doute au debut je voulais utiliser insert mais y aura des doublons nan ? que la ça va ecraser la valeur direct ? à demander en vrai
}

/**
 * @brief buildHashTable: fill the HashTable with given names
 * @param table table to fiil
 * @param names array of names to insert
 * @param namesCount size of names array
 */
void buildHashTable(HashTable& table, std::string* names, int namesCount)
{
    for(unsigned int i=0; i<namesCount; i++){
        table.insert(names[i]);
    }
}

bool HashTable::contains(std::string element) //ça marche pas demander à steeve
{
    // Note: Do not use iteration (for, while, ...)
    int index = hash(element);
    if(this->get(index)== element){
        return true;
    }
    else{
        return false;
    }
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 10;
	w = new HashWindow();
	w->show();

	return a.exec();
}
