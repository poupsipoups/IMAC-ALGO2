#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){
	// selectionSort
   
   
    for(unsigned int j=0; j<toSort.size();j++){
        int minimum =toSort.get(j);
        int count =j;
        for(unsigned int i=j; i<toSort.size();i++){
            if(toSort.get(i)<=minimum){
            minimum=toSort.get(i);
            count =i;
            }
        }

        // if(toSort.get(count)==toSort.get(j)){
        //     toSort.swap(count+1, j);
        // }
        toSort.swap(count,j);
            
    }
        
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
