#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    Noeud* dernier;
    // your code
};

struct DynaTableau{
    int* donnees;
    int nbElements;
    int CAPA_MAX;
    // your code
};


void initialise(Liste* liste)
{
    liste->premier= NULL;
    liste->dernier=NULL;
}

bool est_vide(const Liste* liste)
{
    return liste->premier==NULL;
}

void ajoute(Liste* liste, int valeur)
{
    Noeud* noeud = new Noeud;
    noeud->donnee=valeur;
    if(liste->premier==NULL){
        liste->premier=noeud;
        liste->dernier=noeud;
    }
    else{
        liste->dernier->suivant=noeud;
        liste->dernier=noeud;
    }
}

void affiche(const Liste* liste)
{
    if(est_vide(liste)){
        cout << "La liste est vide."<<endl;
    }

    Noeud* cherche = liste->premier;
    while(cherche != NULL){
        cout << cherche->donnee << endl;
        cherche = cherche->suivant;
    }

}

int recupere(const Liste* liste, int n)
{
    Noeud* cherche = liste-> premier;
    int count=0;
    while(count<n||cherche->suivant==NULL){
        cherche = cherche->suivant;
        count ++;
    }
    if(cherche->suivant==NULL){
            cout<< "Erreur, pas assez d'éléments dans la liste." <<endl;
            return -1;
        }
    return cherche->donnee;
}

int cherche(const Liste* liste, int valeur)
{
    Noeud* cherche = liste->premier;
    int count;
    while(cherche->suivant==NULL){
        if(cherche->donnee==valeur){
            return count;
        }
        else{
            cherche=cherche->suivant;
            count++;  
        }
        
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    int count =0;
    Noeud* cherche = liste->premier;
    while(count<n||cherche->suivant==NULL){
        cherche = cherche->suivant;
        count ++;
    }
    if(cherche->suivant==NULL){
        cout<< "Pas assez d'éléments dans la liste" <<endl;
    }
    else{
        cherche->donnee=valeur;
    }
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if(tableau->nbElements<tableau->CAPA_MAX){
        tableau->donnees[tableau->nbElements] = valeur;
        tableau->nbElements++; 
    }
    else{
        tableau->CAPA_MAX++;
        int* stock =(int*)realloc(tableau->donnees,tableau->CAPA_MAX*sizeof(int));
        if(stock==NULL){
            cout << "erreur mémoire" <<endl;
        }
        else{
            tableau->donnees=stock;
            tableau->donnees[tableau->nbElements]= valeur;
            tableau->nbElements++;

        }
    }
    
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->CAPA_MAX=capacite;
    tableau->nbElements=0;
    tableau->donnees=(int*)malloc(sizeof(int)*capacite);

}

bool est_vide(const DynaTableau* liste)
{
    return liste->nbElements==0;
}

void affiche(const DynaTableau* tableau)
{
    for(int i=0; i<tableau->nbElements;i++){
        cout << tableau->donnees[i] << endl;
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    if(n>tableau->nbElements){
        cout<< "Pas assez d'éléments dans la liste" <<endl;
        return 0;
    }
    else{
        return tableau->donnees[n];
    }

}

int cherche(const DynaTableau* tableau, int valeur)
{
    for(int i=0; i<tableau->nbElements;i++){
        if(tableau->donnees[i]==valeur){
            return i;
        }
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    tableau->donnees[n]=valeur;
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste,valeur);
}

//int retire_file(DynaTableau*liste)
int retire_file(Liste* liste)
{
    Noeud* provisoire = liste->premier;
    liste->premier=liste->premier->suivant;
    return provisoire->donnee;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    ajoute(liste,valeur);
}

int retire_pile(DynaTableau* liste)
//int retire_pile(Liste* liste)
{
    int provisoire = liste->donnees[liste->nbElements-1];
    liste->donnees[liste->nbElements-1]=NULL;
    liste->nbElements--;

    return provisoire;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; 
    DynaTableau pile1;
    Liste file; 
     DynaTableau file1;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file1, i);
        pousse_pile(&pile1, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
